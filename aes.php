<?php

function hash_equals($str1, $str2) {
  if(strlen($str1) != strlen($str2)) {
    return false;
  } else {
    $res = $str1 ^ $str2;
    $ret = 0;
    for($i = strlen($res) - 1; $i >= 0; $i--) $ret |= ord($res[$i]);
    return !$ret;
  }
}

function double_hmac($str1, $str2)
{
  $compare_key = openssl_random_pseudo_bytes(32);
  return hash_hmac('sha256', $str1, $compare_key) === hash_hmac('sha256', $str2, $compare_key);
}

$plaintext = "message to be encrypted";
$ivlen = openssl_cipher_iv_length($cipher="AES-256-CBC");
$iv = openssl_random_pseudo_bytes($ivlen);
$key = "1111111111111111";
$ciphertext_raw = openssl_encrypt($plaintext, $cipher, $key, $options=OPENSSL_RAW_DATA, $iv);
$hmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary=true);
$ciphertext = base64_encode( $iv.$hmac.$ciphertext_raw );
//$hmac = bin2hex($hmac);
print $ciphertext;
print "\n";


$c = base64_decode($ciphertext);
$ivlen = openssl_cipher_iv_length($cipher="AES-256-CBC");
$iv = substr($c, 0, $ivlen);
$hmac = substr($c, $ivlen, $sha2len=32);
$ciphertext_raw = substr($c, $ivlen+$sha2len);
$original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, $key, $options=OPENSSL_RAW_DATA, $iv);
$calcmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary=true);

$hmac = bin2hex($hmac);
$calcmac = bin2hex($calcmac);

print $hmac . "\n";
print $calcmac . "\n";

// Prevent timimg attacks on string comparison
// PHP 5.6+ ==> builtin hash_equals function should be used
// PHP older than 5.6 ==> somebody out there recommends the "hash_equals" in this example
//                        https://www.php.net/manual/en/function.hash-equals.php#115635
//                    ==> perform double hmac as shown in "double_hmac" function
if (hash_equals($hmac, $calcmac))
{
    echo $original_plaintext."\n";
}

if (double_hmac($hmac, $calcmac))
{
    echo $original_plaintext."\n";
}


 ?>
