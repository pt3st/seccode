<?php

if (!isset($argv[1]))
{
  $password = '';
}
else {
  $password = $argv[1];
}

// ^ indicate the start of the string
// $ indicate the end of the string
// match any characters with 8-byte long
$pattern = '/^.{8,}$/';

// match a 8-byte long string containing at least one A-Z
// ?= is a lookahead search which the sub-pattern will be searched and move back to the beginning and start searching the rest again
$pattern = '/^(?=.*[A-Z]).{8,}$/';

// match special chars, digit, A-Z and at least 8 bytes in length
$pattern = '/^(?=.*[!@#$%^&*-])(?=.*[0-9])(?=.*[A-Z]).{8,}$/';

if(preg_match($pattern, $password)){
   echo "Password meets required conditions\n";
} else {
   echo "Password does not required conditions\n";
}

$string = "192.168.1.1";
// IP address matching with filter_var
$valid = filter_var($string, FILTER_VALIDATE_IP);

// IP address matching with regular expression
$valid = preg_match('/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\z/', $string);

?>
