<?php

class Person implements JsonSerializable
{
    public $id;
    public $name;
		public $inject = "test";

    public function __construct(array $data)
    {
        $this->id = $data['id'];
        $this->name = $data['name'];
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

		function __wakeup(){
        if(isset($this->inject)){
            eval($this->inject);
        }
    }

    public function jsonSerialize()
    {
        return
        [
            'id'   => $this->getId(),
            'name' => $this->getName()
        ];
    }
}

/*
# Deserialize data (vulnerability)
//$att = 'a:2:{i:0;s:4:"XVWA";i:1;s:33:"Xtreme Vulnerable Web Application";}';
//$att = 'O:6:"Person":3:{s:2:"id";N;s:4:"name";N;s:6:"inject";s:17:"system(\'whoami\');";}';
$att = 'O:6:"Person":3:{s:2:"id";i:1;s:4:"name";s:4:"Amir";s:6:"inject";s:17:"system(\'whoami\');";}';
$var1 = unserialize($att);
//echo $var1[0];
*/


/*
# Use json_encode and json_decode instead
$person = new Person(array('id' => 1, 'name' => 'Amir'));
$p1 = json_encode($person);
echo $p1 . "\n";

$p2 = json_decode($p1);
echo $p2->id . "\n";
*/


# Create sereialized string to attack
// $person = new Person(array('id' => 1, 'name' => 'Amir'));
// echo serialize($person) ."\n";

 ?>
