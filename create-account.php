<?php
include 'connectioni.php';

echo "<html><body>";
if (!isset($_POST["name"])) {
    echo "Missing name!<br/>";
}
else if (!isset($_POST["email"])) {
    echo "Missing email!<br/>";
}
else if (!isset($_POST["password"])) {
    echo "Missing password!<br/>";
}
else if (!isset($_POST["confpassword"])) {
    echo "Missing confirmation password!<br/>";
}
else if ($_POST["password"] != $_POST["confpassword"]) {
    echo "Passwords did not match!<br/>";
}
else {
    echo "Got account creation request<br>";
    // hash password
    $password = $_POST['password'];
    $bytes = openssl_random_pseudo_bytes( 16 );
    $salt = bin2hex($bytes);
    $hash = hash_pbkdf2("sha512", $password, $salt, 10000, 0);

    $stmt = $link->prepare("INSERT INTO tblMembers (id,username,password,session, name, blog, admin) VALUES(0, ?,?,?, ?, 0, 0);");
    $stmt->bind_param('ssss', $_POST["email"], $hash, $salt, $_POST["name"]);
    $stmt->execute();
    echo 'Account created!';
}

echo "</body></html>";

?>
