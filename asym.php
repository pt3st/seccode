<?php

/* Generate private key and public key

user@seattle:~$ cd files/
user@seattle:~/files$ openssl genrsa -out private.pem 2048
user@seattle:~/files$ openssl rsa -in private.pem -out public.pem -outform PEM -pubout

*/

$path = "/home/user/files";
$public_key = file_get_contents($path . "/public.pem");
$private_key = file_get_contents($path . "/private.pem");

$json_data = "test";
print openssl_public_encrypt($json_data, $encrypted, $public_key, OPENSSL_PKCS1_OAEP_PADDING  )."\n";
echo "Encrypted: " . bin2hex($encrypted) . "\n";

print openssl_private_decrypt($encrypted, $decrypted, $private_key, OPENSSL_PKCS1_OAEP_PADDING )."\n";
echo "Decrypted text : $decrypted\n";

?>
